<?php

namespace App\Http\Controllers;

use App\Feed;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Integer;

class AdminController extends Controller
{
    public function authCheck(Request $request)
    {
        if ($request->get('password') != 0000) {
            redirect('/admin', 403);
        }

        $cookieExpiresAt = time() + 60 * 60;

        setcookie("admin", rand(0, 99999), $cookieExpiresAt);

        return redirect('/feeds');
    }

    public function index()
    {
        if (!isset($_COOKIE["admin"])) {
            redirect('/admin', 403);
        }

        $allFeeds = Feed::query()
            ->get();

        return view('admin', [
            'allFeeds' => $allFeeds
        ]);
    }

    public function create(Request $request)
    {
        if (!isset($_COOKIE["admin"])) {
            redirect('/admin');
        }

        $feed = new Feed();
        $feed->feed = (string)$request->get('feed');
        $feed->url = (string)$request->get('url');
        $feed->save();

        return redirect()->to('/feeds');
    }

    public function destroy(int $id)
    {
        if (!isset($_COOKIE["admin"])) {
            redirect('/admin');
        }

        if (!is_numeric($id) || $id <= 0) {
            redirect('/feeds', 404);
        }

        Feed::query()
            ->findOrFail($id, 'id')
            ->delete();

        return redirect()->back();
    }
}

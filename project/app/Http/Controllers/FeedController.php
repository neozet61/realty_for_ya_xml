<?php

namespace App\Http\Controllers;

use App\Feed;
use Illuminate\Http\Request;

class FeedController extends Controller
{
    public function getFeed(Request $request)
    {
        //получение необходимого XML файла
        $origin = $request->query('origin');

        $feedOnDB = Feed::query()
            ->where('feed','=', $origin)
            ->first();


        if (!$feedOnDB) {
            return response('Invalid origin', 400);
        }

        $arrContextOptions = [
            "ssl" => [
                "verify_peer" => false,
                "verify_peer_name" => false,
            ],
        ];

        $xml = file_get_contents($feedOnDB->url, false, stream_context_create($arrContextOptions));
        $feed = simplexml_load_string($xml);

        // Генерация нового XML
        $newFeed = new \SimpleXMLElement('<offers></offers>');

        foreach ($feed->offer as $offer) {
            // Создайте элемент offer в новом фиде
            $newOffer = $newFeed->addChild('offer');
            $newOffer->addAttribute('internal-id', $offer['internal-id']);

            // Добавляем элементы в новый фид
            $newOffer->addChild('type', $offer->type);
            $newOffer->addChild('property-type', $offer->{"property-type"});
            $newOffer->addChild('category', $offer->category);
            $newOffer->addChild('url', "http://example.com/");
            $newOffer->addChild('creation-date', $offer->{"creation-date"});

            // Добавляем location
            $location = $newOffer->addChild('location');
            $location->addChild('locality-name', $offer->location->{"locality-name"});
            $location->addChild('address', $offer->location->address);

            // Добавляем price
            $newOffer->addChild('price')->addChild('value', $offer->price->value);
            $newOffer->price->addChild('currency', $offer->price->currency);

            // Добавляем информацию о застройщике
            $newOffer->addChild('sales-agent')
                ->addChild('organization', $offer->{"sales-agent"}->organization);

            $newOffer->addChild('floor', $offer->floor);
            $newOffer->addChild('floors-total', $offer->{"floors-total"});

            $newOffer->addChild('area')->addChild('value', $offer->area->value);
            $newOffer->area->addChild('unit', $offer->area->unit);

            $newOffer->addChild('building-name', $offer->{"building-name"});

            // Добавляем image
            foreach ($offer->image as $image) {
                $newOffer->addChild('image', $image);
            }
        }

        $newXml = $newFeed->asXML();

        // Отправляем новый фид XML в качестве ответа
        return response($newXml)->header('Content-Type', 'application/xml');
    }
}

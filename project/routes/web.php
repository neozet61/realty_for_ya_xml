<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Feed;

Route::get('/', function () {
    $allFeeds = Feed::query()
        ->get();

    return view('main', [
        'allFeeds' => $allFeeds
    ]);
});

Route::get('/admin', function () {
    return view('admin-welcome');
});
Route::post('/check_admin', [\App\Http\Controllers\AdminController::class, 'authCheck']);

Route::get('/feeds', [\App\Http\Controllers\AdminController::class, 'index']);

Route::post('/feeds', [\App\Http\Controllers\AdminController::class, 'create']);

Route::get('/feeds/{id}', [\App\Http\Controllers\AdminController::class, 'destroy']);

Route::get('/new-feed', [\App\Http\Controllers\FeedController::class, 'getFeed']);

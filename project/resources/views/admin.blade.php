<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <table>
                    <tr>
                        <th>
                            ID
                        </th>
                        <th>
                            Feed
                        </th>
                        <th>
                            URL
                        </th>
                        <th>
                            Function
                        </th>
                    </tr>
                    @foreach($allFeeds as $feed)
                    <tr>
                        <td>
                            {{$feed->id}}
                        </td>
                        <td>
                            {{$feed->feed}}
                        </td>
                        <td>
                            {{$feed->url}}
                        </td>
                        <td>
                            <a href="/feeds/{{$feed->id}}">[delete]</a>
                        </td>
                    </tr>
                    @endforeach
                </table>
                <br>
                <h2>Add new Feed</h2>
                <form method="post" action="/feeds">
                    @csrf
                    <input type="text" name="feed" placeholder="feed...">
                    <input type="text" name="url" placeholder="URL...">
                    <button>Add</button>
                </form>
            </div>
        </div>
    </body>
</html>
